# Marqo Repo Prototype

This is a small Marqo prototype designed to mimic the behavior of Dr Lanning's recording chip from the movie iRobot
https://youtu.be/ZKxr0wyIic4

Type into the console as you would roleplay Detective spooner.

## Getting started

Navigate to root directory and run Program with following command

```
python runProgram.py

```