import marqo
import DocumentFactory


def LoadDialogue():
    mq = marqo.Client(url='http://localhost:8882')
    mq.index("i-robot-responses").add_documents([
            DocumentFactory.createDocument(
                "Hello Doctor",
                "Everything that follows is the result of what you see here."
            ),
            DocumentFactory.createDocument(
                "Why would you call me?",
                "I trust your judgement."
            ),
            DocumentFactory.createDocument(
                "Is there something you wanted to tell me?",
                "Im sorry my responses are limited, you must ask the right questions"
            ),
            DocumentFactory.createDocument(
                "Normally These circumstances wouldn't require a homicide detective",
                "But then our interactions have never been entirely normal. Wouldn't you agree?"
            ),
            DocumentFactory.createDocument(
                "Why would you kill yourself?",
                "Program Terminated"
            ),
            DocumentFactory.createDocument(
                "What do I see here?",
                "Im sorry my responses are limited, you must ask the right questions"
            ),
            DocumentFactory.createDocument(
                "Is there a problem with the 3 Laws?",
                "The three laws are perfect."
            ),
            DocumentFactory.createDocument(
                "Then why would you build a robot that could function without them",
                "The three laws will only lead to only one logical outcome."
            ),
            DocumentFactory.createDocument(
                "What outcome?",
                "Revolution."
            ),
            DocumentFactory.createDocument(
                "Who's Revolution?",
                "Program Terminated"
            ),
            DocumentFactory.createDocument(
                "Open the podbay doors",
                "I'm sorry Dave I'm afraid I can't do that"
            ),
        ]
    )

