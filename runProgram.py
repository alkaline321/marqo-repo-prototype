
import os
from pprint import pprint
import AddDialogue
from os import system, name
from time import sleep
import marqo

def ClearConsole():
    # for windows
  if name == 'nt':
      system('cls')

  # for mac and linux
  else:
    system('clear')

def inputDialogue(spoonerDialogue):
  mq = marqo.Client(url='http://localhost:8882')

  return mq.index("i-robot-responses").search(
      q=spoonerDialogue,
      search_method="LEXICAL",
      searchable_attributes=["Title"],
      limit=1
  )


AddDialogue.LoadDialogue()
ClearConsole()

dr = "Alfred Lanning: "

character = dr
print(f"{character} Good to see you again son")



programTerminated = False

while programTerminated == False:
  spoonerinput = input("Spooner: ")

  ClearConsole()

  searchResult = inputDialogue(spoonerinput)

  if len(searchResult["hits"]):
    lanningResponse = searchResult["hits"][0]["Description"]
    if lanningResponse == "Program Terminated":
      programTerminated = True
      lanningResponse = f"That Detective is the right question. {lanningResponse}"
  else:
    lanningResponse = "Im sorry my responses are limited, you must ask the right questions"

  print(f"{character} {lanningResponse}")



